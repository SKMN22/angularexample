import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { MatTableModule } from '@angular/material/table';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { TableListComponent } from './components/table-list/table-list.component';
import { TableDialogComponent } from './components/table-dialog/table-dialog.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatDialogModule } from '@angular/material/dialog';
import { MatSortModule } from '@angular/material/sort';
import { GalleryComponent } from './components/gallery/gallery.component';

@NgModule({
  declarations: [AppComponent, TableListComponent, TableDialogComponent, GalleryComponent],
  imports: [
    HttpClientModule,
    MatTableModule,
    BrowserModule,
    BrowserAnimationsModule,
    MatDialogModule,
    MatSortModule 
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
