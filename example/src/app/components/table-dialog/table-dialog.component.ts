import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DummyData } from 'src/app/interfaces/interfaces';

@Component({
  selector: 'app-table-column',
  templateUrl: './table-dialog.component.html',
  styleUrls: ['./table-dialog.component.css'],
})
export class TableDialogComponent {
  constructor(@Inject(MAT_DIALOG_DATA) public data: DummyData) {}

  ngOnInit(): void {}
}
