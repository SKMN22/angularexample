import { Component, OnInit } from '@angular/core';
import { DummyData } from 'src/app/interfaces/interfaces';
import { TableListServiceService } from 'src/app/services/table-list-service.service';
import { MatDialog } from '@angular/material/dialog';
import { TableDialogComponent } from '../table-dialog/table-dialog.component';

@Component({
  selector: 'app-table-list',
  templateUrl: './table-list.component.html',
  styleUrls: ['./table-list.component.css'],
})
export class TableListComponent implements OnInit {
  dataSource: DummyData[] = [];
  displayedColumns: string[] = ['id', 'title', 'rating', 'brand'];
  loaded: boolean = false;

  constructor(
    private readonly tableListService: TableListServiceService,
    private dialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.tableListService.getDummyData().subscribe((data) => {
      this.dataSource = data.products;
      this.loaded = true;
    });
  }

  handleClick(data: DummyData): void {
    this.dialog.open(TableDialogComponent, {
      data,
    });
  }
}
