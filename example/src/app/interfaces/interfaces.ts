export interface Products {
  products: DummyData[];
}

export interface DummyData {
  id: string;
  title: string;
  description: string;
  price: number;
  discountPercentage: number;
  rating: number;
  stock: number;
  brand: string;
  category: number;
  thumbnail: string;
  images: string[];
}
