import { TestBed } from '@angular/core/testing';

import { TableListServiceService } from './table-list-service.service';

describe('TableListServiceService', () => {
  let service: TableListServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TableListServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
