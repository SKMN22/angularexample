import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { Products } from '../interfaces/interfaces';

@Injectable({
  providedIn: 'root',
})
export class TableListServiceService {
  private url: string = 'https://dummyjson.com/products';

  constructor(private http: HttpClient) {}

  getDummyData(): Observable<Products> {
    return this.http.get<Products>(this.url);
  }
}
